# OneDrive

A shameless copy of the Google OAuth package for Outlook (OneDrive) . See the [project page](https://www.meteor.com/accounts) on Meteor Accounts for more details.
