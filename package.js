Package.describe({
  name: "votercircle:meteor-onedrive-oauth",
  summary: "OneDrive OAuth flow",
  version: "1.2.0",
  git: 'https://votercircle.bitbucket.com/votercircle/meteor-onedrive-oauth',
  documentation: 'README.md'
});

Package.on_use(function (api) {
  api.versionsFrom("1.0.1");
  api.use('oauth2', ['client', 'server']);
  api.use('oauth', ['client', 'server']);
  api.use('http', ['server']);
  api.use('underscore', 'client');
  api.use('templating', 'client');
  api.use('random', 'client');
  api.use('service-configuration', ['client', 'server']);

  api.export('OneDrive');

  api.addFiles(
    ['onedrive_configure.html', 'onedrive_configure.js'],
    'client');
  api.add_files('onedrive_server.js', 'server');
  api.add_files('onedrive_client.js', 'client');
});