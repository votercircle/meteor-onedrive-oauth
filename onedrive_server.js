OneDrive = {};

OAuth.registerService('onedrive', 2, null, function (query) {
  var tokens = getTokens(query);
  var accessToken = tokens.access_token;
  var identity = getIdentity(accessToken);
  var expiresAt = (new Date).getTime() / 1000 + tokens.expires_in;

  var response = null;

  if (identity !== null) {
    response = {
      serviceData: {
        id: identity.id,
        oauth_source: identity.userPrincipalName,
        accessToken: OAuth.sealSecret(accessToken),
        email: identity.userPrincipalName,
        username: identity.displayName,
        expiresAt: expiresAt
      },
      options: {
        profile: {
          name: null,
        }
      }
    };
  }
  return response;
});

var userAgent = "Meteor";
if (Meteor.release)
  userAgent += "/" + Meteor.release;

var getTokens = function (query) {
  var config = ServiceConfiguration.configurations.findOne({
    service: 'onedrive'
  });
  if (!config)
    throw new ServiceConfiguration.ConfigError();

  var response;
  try {
    var params = {
      code: query.code,
      client_id: config.clientId,
      client_secret: OAuth.openSecret(config.secret),
      redirect_uri: OAuth._redirectUri('onedrive', config).replace("?close", ""),
      grant_type: 'authorization_code',
      state: query.state
    }
    if (query['session_state']) {
      params['session_state'] = query['session_state'];
    }
    response = HTTP.post(
      "https://login.microsoftonline.com/common/oauth2/v2.0/token", {
        params: params
      });
  } catch (err) {
    throw _.extend(new Error(" ** Failed to complete OAuth handshake with OneDrive. " + err.message), {
      response: err.response
    });
  }
  if (response.data.error) { // if the http response was a json object with an error attribute
    throw new Error(" _ Failed to complete OAuth handshake with OneDrive. " + response.data.error);
  } else {
    return response.data;
  }
};

var getIdentity = function (accessToken) {
  try {
    var response = HTTP.get(
      "https://graph.microsoft.com/v1.0/me/", {
        headers: {
          'Authorization': 'Bearer ' + accessToken,
          'Accept': 'text/*, application/xml, application/json; odata.metadata=none',
          'client-request-id': new Meteor.Collection.ObjectID()._str
        }
      });
    return response.data;
  } catch (err) {
    return null;
  }
};


OneDrive.retrieveCredential = function (credentialToken, credentialSecret) {
  return OAuth.retrieveCredential(credentialToken, credentialSecret);
};